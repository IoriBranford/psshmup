extends KinematicBody

export var speed = 16
var vel = Vector2()
var fire_animation = "fire1"

func _process(delta):
	var r = $Model.rotation.y
	var targetr = -vel.x / speed / 2
	$Model.rotate_y(8*delta*(targetr - r))

func _physics_process(delta):
	var inx = 0
	var inz = 0
	inx += -1 if Input.is_action_pressed("ui_left") else 0
	inx +=  1 if Input.is_action_pressed("ui_right") else 0
	inz += -1 if Input.is_action_pressed("ui_up") else 0
	inz +=  1 if Input.is_action_pressed("ui_down") else 0

	var targetvel = Vector3(inx, 0, inz) * speed
	vel = move_and_slide(targetvel)

	if Input.is_action_just_pressed("ui_select"):
		var animation = $AnimationPlayer.current_animation
		if !animation.begins_with("fire"):
			$AnimationPlayer.play(fire_animation)

func fire_bullet(bullet:PackedScene, angle_deg = 0):
	var angle = deg2rad(angle_deg)
	var num_bullets = 2 if angle != 0 else 1
	for i in range(0, num_bullets):
		var b : RigidBody = bullet.instance()
		b.translation = translation + Vector3.UP
		b.rotation.y = angle
		b.linear_velocity = 64*Vector3.FORWARD.rotated(Vector3.UP, angle)
		get_tree().root.add_child(b)
		angle = -angle

func _on_AnimationPlayer_animation_finished(anim_name:String):
	if anim_name.begins_with("fire"):
		if Input.is_action_pressed("ui_select"):
			$AnimationPlayer.play(fire_animation)
