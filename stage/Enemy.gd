extends RigidBody

export var health = 1
export var hit_sound : AudioStream = preload("res://sound/enemy-hit.wav")
export var die_animation = "die1"
export var speed = 16

func body_entered(collider):
	if (collider.collision_layer & Const.COLLISION_PLAYERSHOT) != 0:
		collider.queue_free()
		health -= 1
		if health >= 1:
			play_sound(hit_sound)
		else:
			$AnimationPlayer.play(die_animation)

func _process(delta):
	var parent = get_parent()
	if parent is PathFollow:
		parent.offset += speed * delta

func play_sound(stream):
	$AudioStreamPlayer.stream = stream
	$AudioStreamPlayer.play()