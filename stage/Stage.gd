extends Node

export var next_stage : PackedScene
var spawns : Dictionary

func _ready():
	var spawnarray = get_tree().get_nodes_in_group("Spawn")
	for spawn in spawnarray:
		spawns[get_path_to(spawn)] = spawn
		spawn.get_parent().remove_child(spawn)
	$AnimationPlayer.play("stage")

func spawn(nodepath:NodePath):
	var spawn = spawns.get(nodepath)
	if spawn:
		add_child(spawn)
