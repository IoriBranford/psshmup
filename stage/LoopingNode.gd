extends Spatial

export var speed = 32
export var loop_z = 16

func _process(delta):
	var z = translation.z
	z += speed * delta
	while z >= loop_z:
		z -= 2*loop_z
	translation.z = z
