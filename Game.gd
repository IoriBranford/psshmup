extends Node

export var first_stage : PackedScene

var stage_scene : PackedScene
var stage : Node

func _ready():
	start_stage(first_stage)

func start_stage(next_stage_scene:PackedScene):
	if stage:
		stage.queue_free()
		remove_child(stage)
		stage = null
	if next_stage_scene:
		stage_scene = next_stage_scene
		stage = stage_scene.instance()
		add_child(stage)
